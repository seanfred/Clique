import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Clients from "./views/Clients.vue";
import Client from "./views/Client.vue";

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/clients",
      name: "clients",
      component: Clients
    },
    {
      path: "/clients/:slug",
      component: Client
    }
  ]
});

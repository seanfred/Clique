const express = require('express')
//const cors = require('cors')
// const util = require("util")
const bodyParser = require('body-parser')
const puppeteer = require('puppeteer')
const schedule = require('node-schedule')

let app = express()
//app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
    res.setHeader('Access-Control-Allow-Credentials', true)
    next()
})

app.listen(80, () => {
    console.log('listening to port localhost:80')
})

app.post('/', async (req, res) => {
    const term = req.body.search
    const rankings = await search(term)
    //res.json(rankings)
})

async function search(term) {
    
    const browser = await puppeteer.launch({
        args: ['--disable-notifications',
                '--no-sandbox',
                '--disable-setuid-sandbox',
                '--disable-infobars'
            ],
            headless: true,
            timeout: 0
    })
    const context = await browser.createIncognitoBrowserContext()
    const page = await context.newPage()
    //await page.bringToFront()
    //await page.setUserAgent('Mozilla/5.0 (iPhone; CPU iPhone OS 11_2_1 like Mac OS X) AppleWebKit/604.4.7 (KHTML, like Gecko) Version/11.0 Mobile/15C153 Safari/604.1')
    // await page.goto('https://www.google.com/', {
    //     waitUntil: 'networkidle2'
    // })
    await page.goto(`https://www.google.com/search?q=${term}&num=1000`, {
        waitUntil: 'networkidle2'
    })


    const results = await page.evaluate((selector) => {
        const lists = document.querySelectorAll(selector)
        const anchors = [...lists]
        return anchors.map((item,i) => {
            return {
                name: item.innerText,
                url: item.href,
                rank: i+1
            }
        })
      }, '.r a')
      const hash = await results.filter(item => {
          return item.name.includes('Hash Interactive')
      })
      await page.click(`.srg .g:nth-child(${hash[0].rank}) a`)
      await delay(70000)

      await page.click('.nav-menu .nav-item:nth-child(1)')
      await delay(15000)

      await page.click('.nav-menu .nav-item:nth-child(2)')
      await delay(30000)

      await page.click('.nav-menu .nav-item:nth-child(3)')
      await delay(50000)

      await page.click('.nav-menu .nav-item:nth-child(4)')
      await delay(20000)

      await page.click('.nav-menu .nav-item:nth-child(5)')
      await delay(45000)

      await page.click('.nav-menu .nav-item:nth-child(6)')
      await delay(15000)

    browser.close()
    //return results

    
    
    //await page.waitForNavigation()
    // await page.type('input[aria-label="Search"]', term, {
    //     delay: 50
    // })
    // await page.keyboard.press('Enter')
    

    
    
    
    
    

    //const html = await page.evaluate(list => list, bodyHandle);
    // const results = await page.$eval(() => {
    //     return document.querySelectorAll('.g')
    // })
   
}

var rule = new schedule.RecurrenceRule()
rule.minute = 10

//var j = schedule.scheduleJob(rule, search('marketing fargo'))

const delay = ms => new Promise(res => setTimeout(res, ms))

var marketing = schedule.scheduleJob('*/2 * * * *', () => {
    search('marketing fargo')
})